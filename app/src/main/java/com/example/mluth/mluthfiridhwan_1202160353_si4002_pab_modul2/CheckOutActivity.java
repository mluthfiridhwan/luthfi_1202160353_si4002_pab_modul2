package com.example.mluth.mluthfiridhwan_1202160353_si4002_pab_modul2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class CheckOutActivity extends AppCompatActivity {
    TextView viewTujuan, viewTanggalBerangkat, viewWaktuBerangkat,
            viewTanggalPulang, viewWaktuPulang, viewJumlahTiket, viewHargaTotal
            , viewSaldo, viewSisaSaldo;

    Button konfirmasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);

        viewTujuan = (TextView)findViewById(R.id.viewTujuan);
        viewTanggalBerangkat = (TextView)findViewById(R.id.viewTanggalBerangkat);
        viewWaktuBerangkat = (TextView)findViewById(R.id.viewWaktuBerangkat);
        viewTanggalPulang = (TextView)findViewById(R.id.viewTanggalPulang);
        viewWaktuPulang= (TextView)findViewById(R.id.viewWaktuPulang);
        viewJumlahTiket = (TextView)findViewById(R.id.viewJumlahTiket);
        viewHargaTotal = (TextView)findViewById(R.id.viewHargaTotal);
        viewSaldo = (TextView)findViewById(R.id.viewSaldo);
        viewSisaSaldo = (TextView)findViewById(R.id.viewSisaSaldo);
        konfirmasi = (Button)findViewById(R.id.btnKonfirmasi);

        Intent in = getIntent();

        String tujuan = in.getStringExtra("daerahTujuan");
        String tanggalb = in.getStringExtra("tanggalBerangkat");
        String waktub = in.getStringExtra("waktuBerangkat");
        String tanggalp = in.getStringExtra("tanggalPulang");
        String waktup = in.getStringExtra("waktuPulang");
        String jumlahTiket = in.getStringExtra("jumlahTiket");
        String hargaTotal = in.getStringExtra("hargaTotal");
        String saldo = in.getStringExtra("saldo");
        String sisasaldo = in.getStringExtra("sisaSaldo");

        viewTujuan.setText(tujuan);
        viewTanggalBerangkat.setText(tanggalb);
        viewWaktuBerangkat.setText(waktub);
        viewTanggalPulang.setText(tanggalp);
        viewWaktuPulang.setText(waktup);
        viewJumlahTiket.setText(jumlahTiket);
        viewHargaTotal.setText(hargaTotal);
        viewSaldo.setText(saldo);
        viewSisaSaldo.setText(sisasaldo);

        konfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Selamat Menikmati Perjalanan Anda...!!!", Toast.LENGTH_LONG).show();
                finish();
                moveTaskToBack(true);
            }
        });

    }
}
